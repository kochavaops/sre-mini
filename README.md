
Ingest traffic from ...
Perform validation and push to a kafka topic
Pull from kafka topic aggregate stats and push to influx

Drop duplicate requests

What do we want to push to influx?

RPS by type (click, install, event)
RPS inside the sliding time window by request type
RPS outside the sliding time window by request type
Number of unique devices by request type
Number of non-unique devices by request type
Difference (in ms) between the time header and the producer processes the request
Difference (in ms) between the time header and the consumer processes the request
Duplicate requests dropped

Dulplicate requests 1% of the time
Duplicate device ids 1% of the time
Time out of window +- 30-60 seconds 1%

{
  "kochava": {
    "`<impression|click|install|event>`": {
      "timestamp": `<unix timestamp>`,
      "id": "`<uuid>`"
    },
    "request": {
      "ip": "`<any valid ip>`",
      "ua": "`<string>`",
      "params": {
        "campaign_id": "`<string right padded('-', 10)><ios|android><hex string 13>`",
        "network_id": "`<integer_range(1,1000)>`",
        "os_version": "`<string>`"
      }
    },
    "device": {
      "id": "`<uuid>`",
      "attribution": {}
    },
    "app": {
      "id": "`<string right padded('-', 10)><ios|android>`",
      "num": "`<integer_range(1,1000)>`",
      "platform": "`<ios|android>`"
    }
  }
}
